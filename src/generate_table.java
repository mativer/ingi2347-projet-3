import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;

public class generate_table extends Thread {
	private static Sha256 outil_sha;
	private final static HashSet<Long> values = new HashSet<Long>();
	public int compteur = 0;
	private static int multiplicateur = 0;

	public static byte[] long32ToBytes(long l) {
		// To write in binary in the file
		// NOTE : We just write 4 byte, because we are limited to 32^2 - 1
		byte[] writeBuffer = new byte[4];
		int i = 0;
		while (i != 4) {
			writeBuffer[i] = (byte) (l >>> 24 - (i * 8));
			i++;
		}
		return writeBuffer;
	}

	public static long bytesToLong32(byte[] reading_buffer) {
		// To read the long in the file
		// WARNING : we have save the long in 4 bytes, not 8 !
		long l = ((reading_buffer[0] & 0xFFL) << 24)
				| ((reading_buffer[1] & 0xFFL) << 16)
				| ((reading_buffer[2] & 0xFFL) << 8)
				| ((reading_buffer[3] & 0xFFL) << 0);
		return l;
	}

	public void generate(int width_chain, int numLine, String name)
			throws IOException {
		DataOutputStream ecrivain;
		ecrivain = new DataOutputStream(new BufferedOutputStream(
				new FileOutputStream(name, true)));

		long value = 0;
		for (int i = 0; i < numLine; i++) {
			outil_sha = new Sha256(multiplicateur);
			if (i % 200 == 0)
				System.out.println("generate : " + i + " lines");
			value++;
			while (outil_sha.hamming_even(value)) {
				value++;
			}

			long result = 0;
			for (int j = 0; j < width_chain; j++) {
				result = Sha256.reduce(outil_sha.sha3(value), j);
			}

			if (values.contains(result)) {
				values.add(result);
				ecrivain.write(long32ToBytes(result));
				ecrivain.write(long32ToBytes(value));
				if (i % 1000 == 0) {
					// Sauvegarde automatique toutes les 1000 lignes
					ecrivain.close();
					ecrivain = new DataOutputStream(new BufferedOutputStream(
							new FileOutputStream(name, true)));
				}
			}
		}
		ecrivain.close();
	}

	public static void main(String[] args) {
		int ligne = 3000;
		int largeur = 300000;
		String name = "Java_dico";
		if (args != null && args.length > 3) {
			ligne = Integer.parseInt(args[1]);
			largeur = Integer.parseInt(args[0]);
			name = args[2];
			multiplicateur = Integer.parseInt(args[3]);
		} else {
			System.out.println("ERREUR : ligne largeur nom mutiplicateur");
			return;
		}
		generate_table rtg = new generate_table();
		outil_sha = new Sha256(multiplicateur);
		try {
			rtg.generate(largeur, ligne, name);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
