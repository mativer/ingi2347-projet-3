import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

public class write_soluce {
	/*
	 * Class to share a table for the solution between all the threads
	 */
	private static String[] table;
	private final String sortie;
	private static String[] entree;

	public write_soluce(int number,String sortie, String[] entree){
		this.sortie = sortie;
		this.entree = entree;
		table = new String[number];
	}
	public boolean is_in_table(int ligne){
		return table[ligne] == null;
	}
	public synchronized void put_in_table(String value, int ligne) throws IOException{
		table[ligne] = value;
		PrintWriter out = new PrintWriter(new FileWriter(sortie));
		String[] resultat = get_table();
		for (int i = 0; i < resultat.length; i++) {
			try {
				out.print(entree[i] + " ");
				if(resultat[i] != null) out.println(resultat[i]);
				else out.println();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		out.close();
	}
	public String[] get_table(){
		return table;
	}
}
