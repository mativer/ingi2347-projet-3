import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

public class Decrypte {
	private static generate_table generate = new generate_table();
	private static long[] tab_long;

	public static HashMap<Long, Long> read_file(String name_dico,
			int numberOfline) throws IOException {
		// Ouvre le fichier et remplit la hashtable
		HashMap<Long, Long> hMonDico = new HashMap<Long, Long>();
		DataInputStream in = new DataInputStream(new FileInputStream(name_dico));
		for (int i = 0; i < numberOfline; i++) {
			byte[] first = new byte[4];
			byte[] last = new byte[4];
			in.read(first);
			in.read(last);
			// We have save the long in 4 b ytes, due to limitation of 2^32 - 1
			hMonDico.put(generate.bytesToLong32(last),
					generate.bytesToLong32(first));
		}
		in.close();
		return hMonDico;
	}

	public static void main(String[] args) throws IOException {
			String[] to_crack = new String[20];
			String name_dico = "Java_dico";
			String sortie = "Result";
			int multiplicateur = 1;
			String name_dico_1; 
			String name_dico_2; 
			String name_dico_3; // = "10000_200000_13.txt";
			String name_dico_4;
			// String sortie = args[0];

			
			if (args != null && args.length > 3) {
				name_dico_1 = args[0];
				name_dico_2 = args[1];
				name_dico_3 = args[2];
				name_dico_4 = args[3];
				sortie = args[4];
			} else {
				System.out
						.println("ERREUR : name_dico_19 name_dico_17 name_dico_13 name_petit_dico_5 sortie");
				return;
			}
			Object[] tab_dico = new Object[4];
			tab_dico[0] = (Object) read_file(name_dico_1, 700000);
			tab_dico[1] = (Object) read_file(name_dico_2, 300000);
			tab_dico[2] = (Object) read_file(name_dico_3, 200000);
			tab_dico[3] = (Object) read_file(name_dico_4, 40000);
			// To prepare the 3 hashmap of the 3 files

			Scanner scanner = new Scanner(new File(sortie));
			int i = 0; // On boucle sur chaque champ detecté
			while (scanner.hasNextLine()) {
				to_crack[i] = scanner.nextLine();
				System.out.println(to_crack[i]);
				i++;
			}
			System.out.println("number of hash : "+i);
			scanner.close();

			/*
			 * First table : Length 700.000, width : 10.000 , multiplicateur :
			 * 19, all the hash to search
			 */
			write_soluce table = new write_soluce(20,sortie,to_crack);
			int nbr_thread = 0;
			thread_search[] tab_search_object = new thread_search[20];
			Thread[] tab_thread = new Thread[20];
			while (nbr_thread != 20) {
				tab_search_object[nbr_thread] = new thread_search(name_dico,
						to_crack, table, nbr_thread + 1,
						(HashMap<Long, Long>) tab_dico[0], 19);
				
				tab_thread[nbr_thread] = new Thread(
						tab_search_object[nbr_thread]);
				
				tab_thread[nbr_thread].start();
				nbr_thread++;
			}
			try {
				int compte_arret = 0;
				while (compte_arret != 20) {
					// wait for the end of all thread
					tab_thread[compte_arret].join();
					compte_arret++;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			/*
			 * Second table : Length 300.000, width : 5000 , multiplicateur : 7,
			 * all the remaining hash from the first table.
			 */
			String[] resultat_after1 = table.get_table();
			nbr_thread = 0;
			tab_search_object = new thread_search[20];
			tab_thread = new Thread[20];
			while (nbr_thread != 20) {
				if (resultat_after1[nbr_thread] != null)
					nbr_thread++;
				else {
					tab_search_object[nbr_thread] = new thread_search(
							name_dico, to_crack, table, nbr_thread + 1,
							(HashMap<Long, Long>) tab_dico[1], 7);
					tab_thread[nbr_thread] = new Thread(
							tab_search_object[nbr_thread]);
					tab_thread[nbr_thread].start();
					nbr_thread++;
				}
			}
			try {
				int compte_arret = 0;
				while (compte_arret != 20) {
					// wait for the end of all thread
					if (tab_thread[compte_arret] != null)
						tab_thread[compte_arret].join();
					compte_arret++;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			/*
			 * 3eme table : Length 200.000, width : 5000 , multiplicateur : 13,
			 * all the remaining hash from the 2 previous table.
			 */
			String[] resultat_after2 = table.get_table();
			nbr_thread = 0;
			tab_search_object = new thread_search[20];
			tab_thread = new Thread[20];
			while (nbr_thread != 20) {
				if (resultat_after2[nbr_thread] != null)
					nbr_thread++;
				else {
					tab_search_object[nbr_thread] = new thread_search(
							name_dico, to_crack, table, nbr_thread + 1,
							(HashMap<Long, Long>) tab_dico[2], 13);
					tab_thread[nbr_thread] = new Thread(
							tab_search_object[nbr_thread]);
					tab_thread[nbr_thread].start();
					nbr_thread++;
				}
			}
			try {
				int compte_arret = 0;
				while (compte_arret != 20) {
					// wait for the end of all thread
					if (tab_thread[compte_arret] != null)
						tab_thread[compte_arret].join();
					compte_arret++;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			/*
			 * 3eme table : Length 40.000, width : 1000 , multiplicateur : 5,
			 * all the remaining hash from the 3 previous table.
			 */
			String[] resultat_after3 = table.get_table();
			nbr_thread = 0;
			tab_search_object = new thread_search[20];
			tab_thread = new Thread[20];
			while (nbr_thread != 20) {
				if (resultat_after2[nbr_thread] != null)
					nbr_thread++;
				else {
					tab_search_object[nbr_thread] = new thread_search(
							name_dico, to_crack, table, nbr_thread + 1,
							(HashMap<Long, Long>) tab_dico[3], 5);
					tab_thread[nbr_thread] = new Thread(
							tab_search_object[nbr_thread]);
					tab_thread[nbr_thread].start();
					nbr_thread++;
				}
			}
			try {
				int compte_arret = 0;
				while (compte_arret != 20) {
					// wait for the end of all thread
					if (tab_thread[compte_arret] != null)
						tab_thread[compte_arret].join();
					compte_arret++;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}


			/*
			 * Write final result in the file, and print on screen
			 */
			PrintWriter out = new PrintWriter(new FileWriter(sortie));
			String[] resultat = table.get_table();
			for (int r = 0; r < resultat.length; r++) {
				try {
					out.print(to_crack[r] + " ");
					if (resultat[r] != null)
						out.println(resultat[r]);
					else
						out.println();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			out.close();
	}
}
