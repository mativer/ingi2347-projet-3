import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Sha256 {
	public static final long MAXSIZE = 4294967295L;
	private static final char[] ALPHABET_HEXA = 
			new char[] { '0', '1', '2', '3','4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };	
	private MessageDigest messageDigest;
	private static int multiplicateur = 1;
	
	/*
	 * Object for the operation for sha256
	 */
	public Sha256(int multiplicateur) {
		this.multiplicateur = multiplicateur;
		// Used in the reduction function
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Error in Sha256 computation");
			e.printStackTrace();
			throw new IllegalStateException(e);
		}
	}

	public byte[] sha256(byte[] password) {
		messageDigest.update(password);
		return messageDigest.digest();
	}

	public byte[] sha3(long l) {
		byte[] hash = sha256(sha256(sha256(ByteBuffer.allocate(4)
				.order(ByteOrder.nativeOrder()).putInt((int) l).array())));
		// Fonction sha256³, program to use our special long in input
		return hash;
	}

	public static long reduce(byte[] hash, int index) {
		long l = ((hashToLong(hash) + (index*23))*multiplicateur) % MAXSIZE;
		// We will take some part of the hash, add somethings to use the property
		// of the rainbow table
		// And we multiplie that by a special number, depending of the table
		// that we are using 
		while (hamming_even(l)) l++;
		// Of course, we check that we have a hamming weight good.
		return l;
	}

	public static long hashToLong(byte[] hash) {
		// Translate the sha 256 hash in long
		long l = ((hash[3] & 0xFFL) << 32) 
					| ((hash[4] & 0xFFL) << 24)
					| ((hash[5] & 0xFFL) << 16) 
					| ((hash[6] & 0xFFL) << 8)
					| ((hash[7] & 0xFFL) << 0);
		return l;
	}

	public static boolean hamming_even(Long l) {
		// Check if our long is valid, regarding the hamming weight
		return Long.bitCount(l) % 2 != 1;
	}

	public static String hashToHexString(byte[] hash) {
		// Translate the hash to an hexadecimal String
		String s = "";
		for (int i = 0; i < hash.length; i++) {
			s += ALPHABET_HEXA[(hash[i] & 0xF0) >> 4];
			s += ALPHABET_HEXA[hash[i] & 0x0F];
		}
		return s;
	}
}
