import java.util.HashMap;
import java.util.Arrays;
import java.io.IOException;


public class thread_search implements Runnable {
	private Sha256 outil_sha;
	private HashMap<Long, Long> hMonDico = new HashMap<Long, Long>();
	private String name_dico;
	private String[] to_find = new String[20];
	public write_soluce writer;
	public int longueur = 0;
	public long timer;
	public int indice;
	public int mult = 0;

	public thread_search(String name, String[] to_search, write_soluce writer,
			int indice, HashMap<Long, Long> hMonDico, int multiplicateur) {
		if(mult == 0){
			outil_sha = new Sha256(19);
		}
		else outil_sha = new Sha256(multiplicateur);
		mult = multiplicateur;
		this.indice = indice;
		this.hMonDico = hMonDico;
		this.name_dico = name;
		this.writer = writer;
		to_find[0] = to_search[indice - 1];
	}

	public long find(int largeur, byte[] hash_to_crack) {
		int i = 0;
		int j = largeur;
		byte[] current_hash = hash_to_crack;
		byte[] maybe_hash;
		long maybe = 0;
		while (j >= 0) {
			if (!writer.is_in_table(indice - 1))
				return 0;
			current_hash = hash_to_crack;
			if (j % 1000 == 0)
				System.out.println(j);
			i = j;
			while (i < largeur) {
				maybe = outil_sha.reduce(current_hash, i);
				if (hMonDico.containsKey(maybe)) {
					int h = 0;
					long start = Long.parseLong(hMonDico.get(maybe).toString());
					while (h <= j + 1) {
						maybe_hash = outil_sha.sha3(start);
						if (Arrays.equals(hash_to_crack, maybe_hash)) {
							return start;
						}
						start = outil_sha.reduce(maybe_hash, h);
						h++;
					}
				}
				current_hash = outil_sha.sha3(maybe);
				i++;
			}
			j--;
		}
		return 0;
	}

	public static byte[] StringToByte(String s) {
		// Translate a crypted pass to a byte[]
		int length_string = s.length();
		int length_string_div = length_string / 2;
		// to avoid float
		byte[] result = new byte[length_string_div];
		for (int i = 0; i < length_string_div; i++) {
			byte x = HexaChar(s.charAt(2 * i));
			x = (byte) (x << 4);
			x |= HexaChar(s.charAt(2 * i + 1));
			result[i] = x;
		}
		return result;
	}

	public static byte HexaChar(char c) {
		// tools to permit the translation of a String in byte[]
		if (c >= '0' && c <= '9') {
			return (byte) (c - '0');
		} else if (c >= 'a' && c <= 'f') {
			return (byte) ((c - 'a') + 0xA);
		} else if (c >= 'A' && c <= 'F') {
			return (byte) ((c - 'A') + 0xA);
		} else {
			throw new IllegalArgumentException();
		}
	}

	public void run() {
		longueur = 0;
		String[] result = new String[20];
		long temp;
		while (longueur != 1) {
			long res;
			if(mult == 5 || mult == 19) res = (temp = find(8000, StringToByte(to_find[longueur])));
			else if(mult == 13) res = (temp = find(9000, StringToByte(to_find[longueur])));
			else res = (temp = find(10000, StringToByte(to_find[longueur])));
			if (res == -1)
				break;
			if (res != 0) {
				result[indice - 1] = Long.toHexString(temp).toLowerCase();
				try {
					writer.put_in_table(result[indice - 1], indice - 1);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("SOLUTION : " + indice + "  est : "+ result[indice - 1]);
			}
			longueur++;
		}
	}
}
