Introduction
Ce projet est le 3eme projet du cours de sécurité : Implémentation et utilisation d'une rainbow table


*************************************Compute the table*************************************************

ATTENTION : Cette étape est TRES longues (plus d'une semaine pour de grandes tables).
VALUE_SPECIFIC : Multiplicateur dans la fonction de réduction, afin de différencier la table des autres.
NBR_LIGNE : Nombre de ligne dans la table
NBR_LARGEUR : Largeur de chaque ligne
FILE_NAME : Nom du fichier de sortie
Pour lancer le programme, veuillez lancer la ligne : 
    => sh ./my_program.sh Generate NBR_LIGNE NBR_LARGEUR FILE_NAME VALUE_SPECIFIC_TO_THIS_TABLE


*************************************Find the password*************************************************

ATTENTION : Ce programme utilise beaucoup de puissance ! 
Pour trouver les mots de passes, utiliser la commande : 
FIRST représente le chemin du fichier contenant la table de 700.000 lignes, largeur 8.000 , multiplicateur : 19
SECOND représente le chemin du fichier contenant la table de 300.000 lignes, largeur 10.000 , multiplicateur : 7
THIRD représente le chemin du fichier contenant la table de 200.000 lignes, largeur 9.000 , multiplicateur : 13
QUATRO représente le chemin du fichier contenant la petite table de 40.000 lignes, largeur 8.000, multiplicateur : 5
INPUT le nom du fichier contenant les 20 hash de départ. il contiendra à la fin les hash + la réponse

	=> ./my_program.sh Decrypte FIRST SECOND THIRD QUATRO INPUT


NOTE IMPORTANTE : 
Ce programme peux bien créer des tables à votre convenances, mais ne peux pas lire d'autres tables que celle fournie de base.
De plus, rappelons que cracker des mots de passes sans accord est interdit, et considérer comme du vol : 
« Utilise ce programme étranger si tel est ton désir
Mais à l'appât du gain, renonce à obéir, 
Car celui qui veut prendre et ne veut pas gagner,
De sa cupidité, le prix devra payer.
Si tu veux t'emparer, en ces lignes souveraines,
D'un trésor convoité qui jamais ne fut tien, 
Voleur tu trouveras, en guise de richesse,
Le juste châtiment de ta folle hardiesse. »
